# Monespace-build

This repository contains the files generated from `ng build`, plus necessary things to make it a Symfony/composer bundle.
The files generated from `ng build` are put into *`Resources/public`*. A Symfony script shall copy/symlink the  content of this directory to *`web/bundles/everycheckmonespace`*.

Add the following to composer.json (the one in the Symfony project):
```json
"repositories": [
  {
      "type": "vcs",
      "url":  "https://bitbucket.org/everycheck/monespace-build.git"
  }
]
```
and
```json
"require": {
    "everycheck/monespace": "dev-master"
},
```
Add this line to AppKernel.php:
```php
// app/AppKernel.php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new everycheck\MonEspaceBundle\everycheckMonEspaceBundle(),
        );

        // ...
    }
}
```
Add/modify the following in .htaccess:
```
# Rewrite all queries beginning with api
RewriteRule api/ %{ENV:BASE}/app.php [L]

# If requested resource exists as a file or directory, skip next two rules
RewriteCond %{REQUEST_FILENAME} -f
RewriteRule ^ - [S=2]

# Requested resource does not exist, do rewrite if it exists in /bundles/everycheckmonespace
RewriteCond %{DOCUMENT_ROOT}/bundles/everycheckmonespace/$1 -f
RewriteRule (.*) %{ENV:BASE}/bundles/everycheckmonespace/$1 [L]

# Rewrite all other queries, but let 404 errors go through for files
RewriteCond %{REQUEST_FILENAME} !.*\.(png|jpg|jpeg|js|html|css|json)
RewriteRule ^ %{ENV:BASE}/bundles/everycheckmonespace/index.html [L]
```